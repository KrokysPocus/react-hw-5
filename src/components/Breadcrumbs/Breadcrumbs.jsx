import { AiOutlineRight } from 'react-icons/ai';
import { Link } from 'react-router-dom';
import styles from './Breadcrumbs.module.scss';
const Breadcrumbs = ({ pathParts }) => {
    return (
        <div className={styles.breadcrumbs}>
            <Link to="/">
                Home <AiOutlineRight />
            </Link>
            {pathParts.map((item, index) => {
                if (index === pathParts.length - 1) {
                    return (
                        <span className={styles.currentPage} key={index}>
                            {item}
                        </span>
                    );
                }
                return (
                    <Link to=".." relative="path" key={index}>
                        {' '}
                        {item} <AiOutlineRight />
                    </Link>
                );
            })}
        </div>
    );
};

export default Breadcrumbs;
