import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import { PatternFormat } from 'react-number-format';

import checkoutValidationSchema from '../../validation/checkoutValidationSchema';
import { clearCartStorageAC } from '../../redux/reducers/cart-reducer';
import { modalSuccessfulCheckOutAC } from '../../redux/reducers/modal-reducer';

import styles from './CheckoutForm.module.scss';

const CheckoutForm = ({ orders, priceDetails }) => {
    const dispatch = useDispatch();
    
    const initialValues = {
        firstName: '',
        lastName: '',
        country: '',
        company: '',
        address: '',
        apartment: '',
        city: '',
        state: '',
        postCode: '',
        phone: ''
    };
    const validationSchema = checkoutValidationSchema();

    const handleSubmit = (values) => {
        const orderDetails = {
            clientName: `${values.firstName} ${values.lastName}`,
            clientPhone: values.phone,
            location: `${values.address}, ${values.city} ${values.postCode}, State: ${values.state}, ${values.country}`,
            company: values.company,
            priceDetails,
            orders
        };

        console.log('We got New order: ', orderDetails);
        dispatch(clearCartStorageAC([]));
        dispatch(modalSuccessfulCheckOutAC(true));
    };

    return (
        <>
            {orders.length > 0 ? (
                <Formik initialValues={initialValues} onSubmit={handleSubmit} validationSchema={validationSchema}>
                    {({ isSubmitting }) => (
                        <Form className={styles.CheckoutForm}>
                            <div className={styles.formField}>
                                <label htmlFor="firstName">First Name*</label>
                                <Field type="text" name="firstName" id="firstName" placeholder="First Name" />
                                <ErrorMessage className={styles.errorField} name="firstName" component="div" />
                            </div>

                            <div className={styles.formField}>
                                <label htmlFor="lastName">Last Name*</label>
                                <Field type="text" name="lastName" id="lastName" placeholder="Last Name" />
                                <ErrorMessage className={styles.errorField} name="lastName" component="div" />
                            </div>

                            <div className={styles.formField}>
                                <label htmlFor="country">Country / Region*</label>
                                <Field type="text" name="country" id="country" placeholder="Country / Region" />
                                <ErrorMessage className={styles.errorField} name="country" component="div" />
                            </div>

                            <div className={styles.formField}>
                                <label htmlFor="company">Company Name</label>
                                <Field type="text" name="company" id="company" placeholder="Company (optional)" />
                                <ErrorMessage className={styles.errorField} name="company" component="div" />
                            </div>

                            <div className={styles.formField}>
                                <label htmlFor="address">Street Address*</label>
                                <Field type="text" name="address" id="address" placeholder="House number and street name" />
                                <ErrorMessage className={styles.errorField} name="address" component="div" />
                            </div>

                            <div className={styles.formField}>
                                <label htmlFor="apartment">Apt, suite, unit</label>
                                <Field type="text" name="apartment" id="apartment" placeholder="apartment, suite, unit, etc. (optional)" />
                                <ErrorMessage className={styles.errorField} name="apartment" component="div" />
                            </div>

                            <div className={styles.locationFields}>
                                <div className={styles.formField}>
                                    <label htmlFor="city">City*</label>
                                    <Field type="text" name="city" id="city" placeholder="Town / City" />
                                    <ErrorMessage className={styles.errorField} name="city" component="div" />
                                </div>

                                <div className={styles.formField}>
                                    <label htmlFor="state">State*</label>
                                    <Field as="select" name="state" id="state">
                                        <option value="State">State</option>
                                        <option value=" Alabama"> Alabama</option>
                                        <option value="kansas">Kansas</option>
                                        <option value="nevada">Nevada</option>
                                    </Field>
                                    <ErrorMessage className={styles.errorField} name="state" component="div" />
                                </div>

                                <div className={styles.formField}>
                                    <label htmlFor="postCode">Postal Code*</label>
                                    <Field type="text" name="postCode" id="postCode" placeholder="Postal Code" />
                                    <ErrorMessage className={styles.errorField} name="postCode" component="div" />
                                </div>
                            </div>

                            <div className={styles.formField}>
                                <label htmlFor="phone">Phone*</label>
                                <Field name="phone">{({ field }) => <PatternFormat {...field} format="+38 (###) ###-####" mask="_" id="phone" placeholder="Phone" />}</Field>
                                <ErrorMessage className={styles.errorField} name="phone" component="div" />
                            </div>
                            <div className={styles.btnWrapper}>
                                <button type="submit" disabled={isSubmitting} className={styles.confirmOrder}>
                                    Continue to delivery
                                </button>
                            </div>
                        </Form>
                    )}
                </Formik>
            ) : (
                <div className={`wrapper ${styles.successfullyCheckout}`}>
                    <p>
                        Your order has been <span>successfully</span> processed
                    </p>
                    <div className={styles.backToShop}>
                        You can back to
                        <Link to="/">Shop</Link>
                    </div>
                </div>
            )}
        </>
    );
};

export default CheckoutForm;
