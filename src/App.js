import { useEffect } from 'react';
import { Routes, Route } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { fetchProductsThunk } from './redux/reducers/products-reducer';

import Modal from './components/Popup/Popup';
import Home from './pages/Home/Home';
import LayOutPage from './pages/LayOutPage/LayOutPage';
import Shop from './pages/Shop/Shop';
import Cart from './pages/Cart/Cart';
import Favorites from './pages/Favorites/Favorites';
import Men from './pages/Men/Men';
import Women from './pages/Women/Women';
import Combos from './pages/Combos/Combos';
import Joggers from './pages/Joggers/Joggers';
import Checkout from './pages/Checkout/Checkout';

import { modalAddToCartAC, modalRemoveFromCartAC } from './redux/reducers/modal-reducer';
import { productsInCartAC } from './redux/reducers/cart-reducer';

import './App.scss';

function App() {
    const dispatch = useDispatch();

    const isModalAddToCart = useSelector((state) => state.modal.isModalAddToCart);
    const isModalRemoveFromCart = useSelector((state) => state.modal.isModalRemoveFromCart);
    const productToAdd = useSelector((state) => state.cart.productToAdd);
    const productToRemove = useSelector((state) => state.cart.productToRemove);
    const productsInCartRedux = useSelector((state) => state.cart.productsInCart);
    const favoritesProducts = useSelector((state) => state.favorites.favoritesInCart);

    const closeHandler = () => {
        if (isModalAddToCart) {
            dispatch(modalAddToCartAC(false));
        } else if (isModalRemoveFromCart) {
            dispatch(modalRemoveFromCartAC(false));
        }
    };

    const updatedProductsInCart = (productId) => {
        const currentProduct = productsInCartRedux.find((item) => item.id === productId.id);
        if (currentProduct) {
            const updatedCart = productsInCartRedux.map((item) => (item.id === productId.id ? { ...item, amount: item.amount + 1 } : item));

            dispatch(productsInCartAC(updatedCart));
        } else {
            const currentCart = [...productsInCartRedux, { ...productId, amount: productId.amount + 1 }];

            dispatch(productsInCartAC(currentCart));
        }
    };

    const addToCard = (productId) => {
        updatedProductsInCart(productId);
        dispatch(modalAddToCartAC(false));
    };

    const deleteProductFromCart = (productId) => {
        const currentProduct = productsInCartRedux.find((item) => item.id === productId);

        if (currentProduct) {
            dispatch(productsInCartAC(productsInCartRedux.filter((item) => item.id !== productId)));
        } else {
            dispatch(productsInCartAC([...productsInCartRedux, productsInCartRedux]));
        }
    };

    const removeFromCart = (productToRemove) => {
        deleteProductFromCart(productToRemove);
        dispatch(modalRemoveFromCartAC(false));
    };

    const changeAmountDecrease = (product) => {
        const currentProduct = productsInCartRedux.find((item) => item.id === product.id);
        const currentAmount = productsInCartRedux.find((item) => item.amount === product.amount);

        if (currentAmount.amount === 1) {
            return dispatch(productsInCartAC(productsInCartRedux));
        }

        if (currentProduct) {
            dispatch(productsInCartAC(productsInCartRedux.map((item) => (item.id === product.id ? { ...item, amount: item.amount - 1 } : item))));
        } else {
            dispatch(productsInCartAC([...productsInCartRedux, { ...product, amount: product.amount - 1 }]));
        }
    };

    const amountDecrease = (product) => {
        changeAmountDecrease(product);
    };

    useEffect(() => {
        dispatch(fetchProductsThunk());
    }, [dispatch]);

    useEffect(() => {
        localStorage.setItem('productsInCart', JSON.stringify(productsInCartRedux));
    }, [productsInCartRedux]);

    useEffect(() => {
        localStorage.setItem('favoriteProductsId', JSON.stringify(favoritesProducts));
    }, [favoritesProducts]);

    return (
        <div className="App">
            <Routes>
                <Route path="/" element={<LayOutPage />}>
                    <Route path="men" element={<Men />} />
                    <Route path="women" element={<Women />} />
                    <Route path="combos" element={<Combos />} />
                    <Route path="joggers" element={<Joggers />} />
                    <Route index element={<Shop />} />
                    <Route path="cart" element={<Cart amountIncrease={addToCard} amountDecrease={amountDecrease} />} />
                    <Route path="cart/checkout" element={<Checkout />} />
                    <Route path="favorites" element={<Favorites addToCard={addToCard} />} />
                </Route>
            </Routes>
            {isModalAddToCart && (
                <Modal
                    header="Add to Card"
                    text="Are you sure at you want to add this product to your card?"
                    isCloseButton
                    closeHandler={closeHandler}
                    actions={
                        <>
                            <button onClick={() => addToCard(productToAdd)}>Add</button>
                            <button onClick={closeHandler}>Cancel</button>
                        </>
                    }
                />
            )}
            {isModalRemoveFromCart && (
                <Modal
                    header="Remove from Card"
                    text="Are you sure at you want remove this product from your card?"
                    isCloseButton
                    closeHandler={closeHandler}
                    actions={
                        <>
                            <button onClick={() => removeFromCart(productToRemove)}>Remove</button>
                            <button onClick={closeHandler}>Cancel</button>
                        </>
                    }
                />
            )}
        </div>
    );
}

export default App;
