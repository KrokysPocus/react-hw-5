const IS_ADD_TO_CART = 'IS_ADD_TO_CART';
const IS_REMOVE_FROM_CART = 'IS_REMOVE_FROM_CART';
const IS_SUCCESSFUL_CHECKOUT = 'IS_SUCCESSFUL_CHECKOUT';
const initialState = {
    isModalAddToCart: false,
    isModalRemoveFromCart: false,
    isModalSuccessfulCheckOut: false
};

const modalReducer = (state = initialState, action) => {
    switch (action.type) {
        case IS_ADD_TO_CART:
            return {
                ...state,
                isModalAddToCart: action.payload
            };
        case IS_REMOVE_FROM_CART:
            return {
                ...state,
                isModalRemoveFromCart: action.payload
            };
        case IS_SUCCESSFUL_CHECKOUT:
            return {
                ...state,
                isModalSuccessfulCheckOut: action.payload
            };
        default:
            return state;
    }
};

export const modalAddToCartAC = (bool) => ({ type: IS_ADD_TO_CART, payload: bool });
export const modalRemoveFromCartAC = (bool) => ({ type: IS_REMOVE_FROM_CART, payload: bool });
export const modalSuccessfulCheckOutAC = (bool) => ({ type: IS_SUCCESSFUL_CHECKOUT, payload: bool });

export default modalReducer;
