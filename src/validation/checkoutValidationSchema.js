import * as Yup from 'yup';

const checkoutValidationSchema = () => {
    return Yup.object().shape({
        firstName: Yup.string()
            .min(2, 'Too Short Name!')
            .max(50, 'Too Long Name!')
            .matches(/^[A-Za-z]+$/, 'First Name cannot contain numbers')
            .required('This field is required*'),
        lastName: Yup.string()
            .min(2, 'Too Short Name!')
            .max(50, 'Too Long Name!')
            .matches(/^[A-Za-z]+$/, 'First Name cannot contain numbers')
            .required('This field is required*'),
        country: Yup.string()
            .matches(/^[A-Za-z]+$/, 'First Name cannot contain numbers')
            .required('This field is required*'),
        company: Yup.string(),
        address: Yup.string()
            .matches(/^[A-Za-z]+\s\d+[A-Za-z]*$/, 'Address should be in the format "Bredgade 25"')
            .required('This field is required*'),
        apartment: Yup.string(),
        city: Yup.string()
            .matches(/^[A-Za-z]+$/, 'City Name cannot contain numbers')
            .required('This field is required*'),
        state: Yup.string().required('This field is required*'),
        postCode: Yup.string()
            .matches(/^[0-9]+$/, 'Postcode must be numeric')
            .required('This field is required*'),
        phone: Yup.string()
            .required('This field is required*')
            .matches(/^\+\d{2} \(\d{3}\) \d{3}-\d{4}$/, 'Invalid phone format')
    });
};

export default checkoutValidationSchema;
